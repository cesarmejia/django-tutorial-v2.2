# Django Tutorial v2.2

## Section

- [x] Part 01: Up and Going
- [x] Part 02: DB, Models, Admin Scaffold
- [x] Part 03: Views, Templates, HttpResponse vs Http404, helper methods, Namespacing URLs
- [x] Part 04: Simple form, Generic Views
- [x] Part 05: Automated tests, Basic test, Running tests, Test a view
- [x] Part 06: Loading static resources (images, css, etc.)
- [x] Part 07: Customizing admin form
- [x] Tutorial Complete
